//
//  AppDelegate.h
//  My WorkMates
//
//  Created by Sergio Calle on 11/26/14.
//  Copyright (c) 2014 Sergio Calle. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate, UIAlertViewDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (nonatomic, strong) NSDictionary *dictionary;

@end

