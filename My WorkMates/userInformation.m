//
//  userInformation.m
//  My WorkMates
//
//  Created by Sergio Calle on 11/26/14.
//  Copyright (c) 2014 Sergio Calle. All rights reserved.
//

#import "userInformation.h"

@implementation userInformation

@synthesize usr_cellular, usr_birthday,usr_address,usr_email,usr_role,dep_uid,usr_city,usr_country,usr_create_date,usr_due_date,usr_fax,usr_firstname,usr_lastname,usr_location,usr_password,usr_phone,usr_position,usr_replaced_by,usr_reports_to,usr_resume,usr_status,usr_uid,usr_update_date,usr_username,usr_ux,usr_zip_code;


-(NSArray *)getListOfKeysForClass{
    
    NSArray *arrayKeys = @[@"usr_cellular", @"dep_uid", @"usr_birthday", @"usr_address", @"usr_email", @"usr_role", @"usr_city", @"usr_country", @"usr_create_date", @"usr_due_date", @"usr_fax", @"usr_firstname", @"usr_lastname", @"usr_location", @"usr_password", @"usr_phone", @"usr_position", @"usr_replaced_by", @"usr_reports_to", @"usr_resume", @"usr_status", @"usr_uid", @"usr_update_date", @"usr_username", @"usr_ux", @"usr_zip_code"];
    return arrayKeys;
}

@end
