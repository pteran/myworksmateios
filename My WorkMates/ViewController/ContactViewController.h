//
//  ContactViewController.h
//  My WorkMates
//
//  Created by Sergio Calle on 11/26/14.
//  Copyright (c) 2014 Sergio Calle. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "userInformation.h"
#import "InformationViewController.h"
#import "SyncronizeViewController.h"


@interface ContactViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) NSArray *arrayElements;
@property (strong, nonatomic) IBOutlet UITableView *tableViewList;

@end
