//
//  SyncronizeViewController.m
//  My WorkMates
//
//  Created by Sergio Calle on 11/27/14.
//  Copyright (c) 2014 Sergio Calle. All rights reserved.
//

#import "SyncronizeViewController.h"

@interface SyncronizeViewController ()

@end

@implementation SyncronizeViewController

@synthesize listUsers;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.imgUser.layer.cornerRadius = self.imgUser.frame.size.width / 2;
    self.imgUser.layer.borderWidth = 3.0f;
    self.imgUser.layer.borderColor = [UIColor grayColor].CGColor;
    self.imgUser.clipsToBounds = YES;
    
    self.viewLoggin.layer.cornerRadius = 12;
    self.viewLoggin.clipsToBounds = YES;
    
    self.btnLogout.layer.cornerRadius = 12;
    self.btnSyncroni.layer.cornerRadius = 12;
    
    [self.llbUserName setText:[[NSUserDefaults standardUserDefaults] objectForKey:@"userName"]];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)SyncronizeContacts:(id)sender {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"My WorksMate" message:@"Do you want syncronize this contacts with the iPhone list" delegate:self cancelButtonTitle:@"Syncronize" otherButtonTitles:@"Cancel", nil];
    [alert show];
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0) {
        [self performSelectorInBackground:@selector(showLoadingView) withObject:nil];
        [self CopyContactsToAddressBook];
    }
}


-(void)showLoadingView{
    self.viewLoggin.hidden = NO;
    [self.activityIndicator startAnimating];
    
}

-(void)CopyContactsToAddressBook{
    
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusDenied ||
        ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusRestricted){
        //1
        NSLog(@"Denied");
        UIAlertView *cantAddContactAlert = [[UIAlertView alloc] initWithTitle: @"Cannot Add Contact" message: @"You must give the app permission to add the contact first." delegate:nil cancelButtonTitle: @"OK" otherButtonTitles: nil];
        [cantAddContactAlert show];
    } else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized){
        //2
        NSLog(@"Authorized");
        [self starCopying];
    } else{ //ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined
        //3
        NSLog(@"Not determined");
        ABAddressBookRequestAccessWithCompletion(ABAddressBookCreateWithOptions(NULL, nil), ^(bool granted, CFErrorRef error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (!granted){
                    //4
                    UIAlertView *cantAddContactAlert = [[UIAlertView alloc] initWithTitle: @"Cannot Add Contact" message: @"You must give the app permission to add the contact first." delegate:nil cancelButtonTitle: @"OK" otherButtonTitles: nil];
                    [cantAddContactAlert show];
                    return;
                }
                //5
                [self starCopying];
            });
        });
    }
}

-(void)starCopying{
    
    for (int index = 0; index < [self.listUsers count] ; index++) {
        
        BOOL existRegister = NO;
        
        NSDictionary *dictionary = [listUsers objectAtIndex:index];
        
        userInformation *user = [[userInformation alloc] init];
        
        [user setValuesForKeysWithDictionary:dictionary];
        
        ABAddressBookRef addressBookRef = ABAddressBookCreateWithOptions(NULL, nil);
        ABRecordRef pet = ABPersonCreate();
        ABRecordSetValue(pet, kABPersonFirstNameProperty, (__bridge CFStringRef)user.usr_firstname, nil);
        ABRecordSetValue(pet, kABPersonLastNameProperty, (__bridge CFStringRef)user.usr_lastname, nil);
        
        ABMutableMultiValueRef multiEmail = ABMultiValueCreateMutable(kABMultiStringPropertyType);
        ABMultiValueAddValueAndLabel(multiEmail, (__bridge CFTypeRef)(user.usr_email), kABWorkLabel, NULL);
        ABRecordSetValue(pet, kABPersonEmailProperty, multiEmail, nil);
        
        ABMutableMultiValueRef phoneNumbers = ABMultiValueCreateMutable(kABMultiStringPropertyType);
        ABMultiValueAddValueAndLabel(phoneNumbers, (__bridge CFStringRef)user.usr_phone, kABPersonPhoneMainLabel, NULL);
        ABRecordSetValue(pet, kABPersonPhoneProperty, phoneNumbers, nil);
        
        UIImage *imageData = [UIImage imageNamed:@"user.png"];
        
        NSData *data = UIImagePNGRepresentation(imageData);
        
        ABPersonSetImageData(pet, (__bridge CFDataRef)data, nil);
        ABAddressBookAddRecord(addressBookRef, pet, nil);
        
        NSArray *allContacts = (__bridge NSArray *)ABAddressBookCopyArrayOfAllPeople(addressBookRef);
        
        for (id record in allContacts){
            ABRecordRef thisContact = (__bridge ABRecordRef)record;
            if (CFStringCompare(ABRecordCopyCompositeName(thisContact),
                                ABRecordCopyCompositeName(pet), 0) == kCFCompareEqualTo){
                //The contact already exists!
                existRegister = YES;
                break;
            }
        }
        if (!existRegister)
            ABAddressBookSave(addressBookRef, nil);
    }
    
    
    self.viewLoggin.hidden = YES;
    [self.activityIndicator stopAnimating];
    
    UIAlertView *contactAddedAlert = [[UIAlertView alloc]initWithTitle:@"Contacts Added" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [contactAddedAlert show];
    
}

@end
