//
//  ContactViewController.m
//  My WorkMates
//
//  Created by Sergio Calle on 11/26/14.
//  Copyright (c) 2014 Sergio Calle. All rights reserved.
//

#import "ContactViewController.h"

@interface ContactViewController ()

@end

@implementation ContactViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"http://192.168.0.121/api/1.0/nativexspeed/users"] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:9];
    
    [request setHTTPMethod:@"GET"];
    
    NSDictionary *dictinary = [[NSUserDefaults standardUserDefaults] objectForKey:@"tokenInformation"];
    
    [request setValue:[NSString stringWithFormat:@"Bearer %@", [dictinary objectForKey:@"access_token"]] forHTTPHeaderField:@"Authorization"];
    
    NSURLResponse *response;
    NSError *error;
    
    NSData *dataResult = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    self.arrayElements = [[NSArray alloc] init];
    
    self.arrayElements = [NSJSONSerialization JSONObjectWithData:dataResult options:NSJSONReadingAllowFragments error:nil];
    
}

#pragma mark Instance tableview options.

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.arrayElements count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIdentifier = @"cellListContact";
    
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    userInformation *user = [[userInformation alloc] init];
    
    NSDictionary *dictionary = [self.arrayElements objectAtIndex:indexPath.row];
    
    [user setValuesForKeysWithDictionary:dictionary];
    
    UILabel *lblName = (UILabel *)[cell viewWithTag:101];
    
    [lblName setText:[NSString stringWithFormat:@"%@ %@", user.usr_firstname, user.usr_lastname]];
    
    UILabel *lblPosition = (UILabel *)[cell viewWithTag:102];
    
    [lblPosition setText:user.usr_role];
    
    UILabel *lblCellularName = (UILabel *)[cell viewWithTag:103];
    
    [lblCellularName setText:user.usr_phone];
    
    
    return cell;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"SettingSegue"]) {
        
        SyncronizeViewController *destinoViewController = [segue destinationViewController];
        destinoViewController.listUsers = self.arrayElements;
        
    }else{
        NSIndexPath *indexPath = [self.tableViewList indexPathForSelectedRow];
        
        NSDictionary *dictionary = [self.arrayElements objectAtIndex:indexPath.row];
        
        userInformation *user = [[userInformation alloc] init];
        
        [user setValuesForKeysWithDictionary:dictionary];
        
        InformationViewController *destinoViewController = [segue destinationViewController];
        
        destinoViewController.user = user;
        
        destinoViewController.title = @"Información";
    }
}


@end
