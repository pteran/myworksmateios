//
//  InformationViewController.h
//  My WorkMates
//
//  Created by Sergio Calle on 11/26/14.
//  Copyright (c) 2014 Sergio Calle. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "userInformation.h"

@interface InformationViewController : UIViewController<UITextFieldDelegate>

@property (nonatomic, strong) userInformation *user;

@property (strong, nonatomic) IBOutlet UIImageView *imgUser;

@property (strong, nonatomic) IBOutlet UILabel *lblUserName;

@property (strong, nonatomic) IBOutlet UILabel *lblRole;

@property (strong, nonatomic) IBOutlet UITextView *txtViewCellPhoneName;

@property (strong, nonatomic) IBOutlet UITextView *txtViewAdress;

@property (strong, nonatomic) IBOutlet UITextView *txtViewBirthday;

@property (strong, nonatomic) IBOutlet UITextView *txtViewEmail;

@property (nonatomic) BOOL isEditing;

- (IBAction)callUser:(id)sender;

- (IBAction)sendMessage:(id)sender;

- (IBAction)EdithOrSaveInformation:(UIBarButtonItem *)sender;

@property (strong, nonatomic) IBOutlet UIView *contentView;


@end
