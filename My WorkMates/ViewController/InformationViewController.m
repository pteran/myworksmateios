//
//  InformationViewController.m
//  My WorkMates
//
//  Created by Sergio Calle on 11/26/14.
//  Copyright (c) 2014 Sergio Calle. All rights reserved.
//

#import "InformationViewController.h"

@interface InformationViewController ()

@end

@implementation InformationViewController

@synthesize user;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    
    // Do any additional setup after loading the view.
    
    [self.lblUserName setText:[NSString stringWithFormat:@"%@ %@", user.usr_firstname, user.usr_lastname]];
    
    [self.lblRole setText:[NSString stringWithFormat:@"%@", user.usr_role]];
    
    [self.txtViewCellPhoneName setText:user.usr_phone];
    
    [self.txtViewAdress setText:user.usr_address];
    
    [self.txtViewBirthday setText:user.usr_birthday];
    
    [self.txtViewEmail setText:user.usr_email];
    
    self.imgUser.layer.cornerRadius = self.imgUser.frame.size.width / 2;
    self.imgUser.layer.borderWidth = 3.0f;
    self.imgUser.layer.borderColor = [UIColor grayColor].CGColor;
    self.imgUser.clipsToBounds = YES;
    
    [self.imgUser setImage:[UIImage imageNamed:@"user.png"]];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)callUser:(id)sender {
    NSString *phNo = user.usr_phone;
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",phNo]];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    } else
    {
        UIAlertView *calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Call facility is not available!!!" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [calert show];
    }
}

- (IBAction)sendMessage:(id)sender {
    
    NSLog(@"sendMsgBtnPressed");
    
    NSString *stringURL = [NSString stringWithFormat:@"sms:%@", user.usr_phone];
    NSURL *url = [NSURL URLWithString:stringURL];
    [[UIApplication sharedApplication] openURL:url];
    
}

- (IBAction)EdithOrSaveInformation:(UIBarButtonItem *)sender {
    
    if (self.isEditing) {
        self.isEditing = NO;
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(EdithOrSaveInformation:)];
        // Implement code to submit the changes to WS.
        
        // Value Changed.
        
        BOOL valueChanged = NO;
        
        if (![user.usr_email isEqualToString:self.txtViewEmail.text]) {
            user.usr_email = self.txtViewEmail.text;
            valueChanged = YES;
        }else if (![user.usr_address isEqualToString:self.txtViewAdress.text]){
            user.usr_address = self.txtViewAdress.text;
            valueChanged = YES;
        }else if (![user.usr_birthday isEqualToString:self.txtViewBirthday.text]){
            user.usr_birthday = self.txtViewBirthday.text;
            valueChanged = YES;
        }else if (![user.usr_phone isEqualToString:self.txtViewCellPhoneName.text]){
            user.usr_phone = self.txtViewCellPhoneName.text;
            valueChanged = YES;
        }
        
        if (valueChanged) {
            
            NSDictionary *dictionary = [user dictionaryWithValuesForKeys:[user getListOfKeysForClass]];
            // Send Information
            [self SendNewInformation:dictionary];
        }
        
//        [self.txtViewEmail setTextColor:[UIColor blackColor]];
//        [self.txtViewAdress setTextColor:[UIColor blackColor]];
//        [self.txtViewBirthday setTextColor:[UIColor blackColor]];
//        [self.txtViewCellPhoneName setTextColor:[UIColor blackColor]];
        
        [self.txtViewEmail setEditable:NO];
        [self.txtViewAdress setEditable:NO];
        [self.txtViewBirthday setEditable:NO];
        [self.txtViewCellPhoneName setEditable:NO];
        
    }else{
        
        self.isEditing = YES;
        
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(EdithOrSaveInformation:)];
        
//        [self.txtViewEmail setTextColor:[UIColor redColor]];
//        [self.txtViewAdress setTextColor:[UIColor redColor]];
//        [self.txtViewBirthday setTextColor:[UIColor redColor]];
//        [self.txtViewCellPhoneName setTextColor:[UIColor redColor]];
        
        [self.txtViewEmail setEditable:YES];
        [self.txtViewAdress setEditable:YES];
        [self.txtViewBirthday setEditable:YES];
        [self.txtViewCellPhoneName setEditable:YES];
    }
}

-(void)SendNewInformation:(NSDictionary *)dictionary{
    
    NSURLResponse *response;
    NSError *error;
    
    NSString *stringUrl = [[NSString alloc] initWithFormat:@"http://192.168.0.121/api/1.0/nativexspeed/user/phone/%@", user.usr_uid];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:stringUrl] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:20];
    
    NSDictionary *dictinary = [[NSUserDefaults standardUserDefaults] objectForKey:@"tokenInformation"];
    
    [request setValue:[NSString stringWithFormat:@"Bearer %@", [dictinary objectForKey:@"access_token"]] forHTTPHeaderField:@"Authorization"];
    
    NSData *sent = [NSJSONSerialization dataWithJSONObject:dictionary options:NSJSONWritingPrettyPrinted error:nil];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:sent];
    
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    NSString *string = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    NSLog(@"%@ ja %@ ja %@", string, response, error);
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
    [self.txtViewAdress resignFirstResponder];
    [self.txtViewBirthday resignFirstResponder];
    [self.txtViewCellPhoneName resignFirstResponder];
    [self.txtViewEmail resignFirstResponder];
}


- (void)keyboardWillShow:(NSNotification *)note {
    NSDictionary *userInfo = note.userInfo;
    NSTimeInterval duration = [userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    UIViewAnimationCurve curve = [userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue];
    
    CGRect keyboardFrameEnd = [userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    keyboardFrameEnd = [self.view convertRect:keyboardFrameEnd fromView:nil];
    
    [UIView animateWithDuration:duration delay:0 options:UIViewAnimationOptionBeginFromCurrentState | curve animations:^{
        self.contentView.frame = CGRectMake(0, 0, keyboardFrameEnd.size.width, keyboardFrameEnd.origin.y);
    } completion:nil];
}

- (void)keyboardWillHide:(NSNotification *)note {
    NSDictionary *userInfo = note.userInfo;
    NSTimeInterval duration = [userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    UIViewAnimationCurve curve = [userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue];
    
    CGRect keyboardFrameEnd = [userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    keyboardFrameEnd = [self.view convertRect:keyboardFrameEnd fromView:nil];
    
    [UIView animateWithDuration:duration delay:0 options:UIViewAnimationOptionBeginFromCurrentState | curve animations:^{
        self.contentView.frame = CGRectMake(0, 0, keyboardFrameEnd.size.width, keyboardFrameEnd.origin.y);
    } completion:nil];
}

@end
