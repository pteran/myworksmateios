//
//  SyncronizeViewController.h
//  My WorkMates
//
//  Created by Sergio Calle on 11/27/14.
//  Copyright (c) 2014 Sergio Calle. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import "userInformation.h"

@interface SyncronizeViewController : UIViewController <UIAlertViewDelegate>

- (IBAction)SyncronizeContacts:(id)sender;

@property (nonatomic, strong) NSArray *listUsers;

@property (strong, nonatomic) IBOutlet UILabel *llbUserName;

@property (strong, nonatomic) IBOutlet UIImageView *imgUser;

@property (strong, nonatomic) IBOutlet UIButton *btnSyncroni;

@property (strong, nonatomic) IBOutlet UIButton *btnLogout;

@property (strong, nonatomic) IBOutlet UIView *viewLoggin;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end
