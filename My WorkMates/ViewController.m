//
//  ViewController.m
//  My WorkMates
//
//  Created by Sergio Calle on 11/26/14.
//  Copyright (c) 2014 Sergio Calle. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.viewLogin.layer.cornerRadius = 12;
    self.viewLogin.clipsToBounds = YES;
    
    // Configure the necesary part for webView.
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)actionLogin:(id)sender {
    
    self.correctLoginProcess = NO;
    
    NSString *post =[NSString stringWithFormat:@"grant_type=password&username=%@&password=%@", self.txtFieldName.text, self.txtFieldUser.text];
    
    NSString *postLength = [NSString stringWithFormat:@"%d",[post length]];
    
    NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"http://192.168.0.121/nativexspeed/oauth2/token"] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:30];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"Basic VkZUS1ZaSkxUUUFaWUREWFBYU0FEQVhTWUdYWktMWEc6NjU3Njc4OTg3NTQ3NWUwMDNlYWNkYTEwOTQwNjcwMjk" forHTTPHeaderField:@"Authorization"];
    
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    NSURLResponse *response;
    NSError *error;
    
    NSData *dataResult = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    if (dataResult)
        self.correctLoginProcess = YES;
    else{
        self.correctLoginProcess = NO;
        return;
    }
    
    NSDictionary *dictionaryError = [NSJSONSerialization JSONObjectWithData:dataResult options:NSJSONReadingAllowFragments error:nil];
    
    if ([dictionaryError objectForKey:@"error"]) {
        self.correctLoginProcess = NO;
        return;
    }
    
    NSString *string = [[NSString alloc] initWithData:dataResult encoding:NSUTF8StringEncoding];
    
    NSLog(@"Viendo %@ m   %@     %@", string, response, error);
    
    NSMutableDictionary *dictinary = [[NSMutableDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:dataResult options:NSJSONReadingAllowFragments error:nil]];
    
    [dictinary setObject:@"null" forKey:@"scope"];
    
    NSDictionary *dictinaryToken = [[NSDictionary alloc] initWithDictionary:dictinary];
    
    [[NSUserDefaults standardUserDefaults]setObject:dictinaryToken forKey:@"tokenInformation"];
    
    [[NSUserDefaults standardUserDefaults] setObject:self.txtFieldName.text forKey:@"userName"];
    
}

#pragma mark Managements of views

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    
    if (self.correctLoginProcess)
        return YES;
    else{
        UIAlertView *alerta = [[UIAlertView alloc] initWithTitle:@"My Worksmate" message:@"Your user or password are incorrect." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alerta show];
        return NO;
    }
}


@end
