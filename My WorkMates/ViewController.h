//
//  ViewController.h
//  My WorkMates
//
//  Created by Sergio Calle on 11/26/14.
//  Copyright (c) 2014 Sergio Calle. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UIWebViewDelegate>

@property (strong, nonatomic) IBOutlet UITextField *txtFieldName;
@property (strong, nonatomic) IBOutlet UITextField *txtFieldUser;

- (IBAction)actionLogin:(id)sender;

@property (nonatomic) BOOL correctLoginProcess;

@property (strong, nonatomic) IBOutlet UIView *viewLogin;
@end

