//
//  userInformation.h
//  My WorkMates
//
//  Created by Sergio Calle on 11/26/14.
//  Copyright (c) 2014 Sergio Calle. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface userInformation : NSObject

@property (nonatomic, strong) NSString *usr_uid;
@property (nonatomic, strong) NSString *usr_username;
@property (nonatomic, strong) NSString *usr_password;
@property (nonatomic, strong) NSString *usr_firstname;
@property (nonatomic, strong) NSString *usr_lastname;
@property (nonatomic, strong) NSString *usr_email;
@property (nonatomic, strong) NSString *usr_due_date;
@property (nonatomic, strong) NSString *usr_create_date;
@property (nonatomic, strong) NSString *usr_update_date;
@property (nonatomic, strong) NSString *usr_status;
@property (nonatomic, strong) NSString *usr_country;
@property (nonatomic, strong) NSString *usr_city;
@property (nonatomic, strong) NSString *usr_location;
@property (nonatomic, strong) NSString *usr_address;
@property (nonatomic, strong) NSString *usr_phone;
@property (nonatomic, strong) NSString *usr_fax;
@property (nonatomic, strong) NSString *usr_cellular;
@property (nonatomic, strong) NSString *usr_zip_code;
@property (nonatomic, strong) NSString *dep_uid;
@property (nonatomic, strong) NSString *usr_position;
@property (nonatomic, strong) NSString *usr_resume;
@property (nonatomic, strong) NSString *usr_birthday;
@property (nonatomic, strong) NSString *usr_role;
@property (nonatomic, strong) NSString *usr_reports_to;
@property (nonatomic, strong) NSString *usr_replaced_by;
@property (nonatomic, strong) NSString *usr_ux;

-(NSArray *)getListOfKeysForClass;

@end
